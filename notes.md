

  - Intro Finite-Differences & Error: realize that the in some range of
    steps, the analysis of Taylor expansion is the best predictor of the
    error, but that it does not work for the values of the step that are
    too small.

    (30 min)

  - Round-off for floating-point (decimal) numbers. 
    The approximation function is idempotent and the relative error is
    controlled by the machine epsilon.

    (20 min)

  - Round-off error of FD: quantitative approach.

    (10 min)

  - CSD

    (20 min)

  - Exo

    (10 min)

Optional:

  - binary model of floating-point numbers.

  - central difference / higher-order schemes.

  - exo "iteration of CSD doesn't work".

  - intro to spectral method: use Cauchy integral formula to get the n-th
    derivative (approx via a Riemann sum).
